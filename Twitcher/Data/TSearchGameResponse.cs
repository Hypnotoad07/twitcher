﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Twitcher.Data
{
    public class LinksSearchResponse
    {
        public string self { get; set; }
    }

    public class TSearchGameResponse
    {
        public LinksSearchResponse _links { get; set; }
        public List<TGame> games = new List<TGame>();

        public bool IsEmpty { get { return games.Count == 0; } }

        public TSearchGameResponse()
        { }

        public TSearchGameResponse(string content)
        {
            var tResponse = JsonConvert.DeserializeObject<TSearchGameResponse>(content);
            this._links = tResponse._links;
            this.games = tResponse.games;
        }
    }
}