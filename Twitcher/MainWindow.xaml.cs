﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using RestSharp;
using RestSharp.Contrib;
using Twitcher.Data;

namespace Twitcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnSearchChange(object sender, TextChangedEventArgs e)
        {
            // if not found set bg of textbox in RED
            // if found clear & repopulate the listbox

            var tb = (TextBox) sender;
            SearchGame(tb.Text);
        }

        private void OnGameSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbGames.Items == null) return;
            if (lbGames.Items.Count == 0) return;

            var tGame = (TGame)lbGames.SelectedItems[0];

            if (tGame == null) return;

            GetStreams(tGame.name);
        }

        private void OnPlayVlc_Click(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag;

            var p = new Process();
            try
            {
                p.StartInfo.UseShellExecute = false;

                p.StartInfo.FileName = @"C:\Program Files (x86)\Livestreamer\livestreamer.exe";
                p.StartInfo.Arguments = tag + " best";
                p.StartInfo.CreateNoWindow = true;
                p.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void OnOpenChat_Click(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag;

            Process.Start(tag.ToString());
        }

        public void SearchGame(string query)
        {
            var client = new RestClient(Twitch.BASE_URL);
            var request = new RestRequest("search/games?q={query}&type=suggest", Method.GET);
            request.AddHeader("Accept", Twitch.ACCEPT_HEADER);
            request.AddUrlSegment("query", query);

            var response = client.ExecuteAsync(request, r =>
            {
                if (r.ResponseStatus == ResponseStatus.Completed)
                {

                    var content = r.Content; // raw content as string
                    if (content == null) return;
                    if(content.Equals("")) return;
                    if(content.Length == 0) return;

                    var tResponse = new TSearchGameResponse(content);

                    if (tResponse.IsEmpty)
                    {
                        Dispatcher.Invoke(() => tbSearchGame.Background = new SolidColorBrush(Colors.Red));
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            tbSearchGame.Background = new SolidColorBrush(Colors.Green);
                            lbGames.Items.Clear();

                            foreach (var game in tResponse.games)
                            {
                                lbGames.Items.Add(game);
                            }
                        });
                    }
                }
            });
        }

        public void GetStreams(string game)
        {
            var client = new RestClient(Twitch.BASE_URL);
            var request = new RestRequest("streams?game={game}", Method.GET);
            request.AddHeader("Accept", Twitch.ACCEPT_HEADER);
            request.AddUrlSegment("game", game);

            var response = client.ExecuteAsync(request, r =>
            {
                if (r.ResponseStatus == ResponseStatus.Completed)
                {

                    var content = r.Content; // raw content as string
                    if (content == null) return;
                    if (content.Equals("")) return;
                    if (content.Length == 0) return;
                
                    var tResponse = new TStreamsResponse(content);

                    if (!tResponse.IsEmpty)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            lbStreams.Items.Clear();

                            foreach (var stream in tResponse.streams)
                            {
                                lbStreams.Items.Add(stream);
                            }
                        });
                    }
                }
            });

        }
    }
}
